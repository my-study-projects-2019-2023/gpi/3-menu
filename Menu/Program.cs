﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            int choice = 0;
            while(choice == 0)
            {
                Console.WriteLine("Bitte gebe deine Menü Auswahl ein (1, 2, 3; für Ende die 9):");
                string input = Console.ReadLine();
                int enteredNumber = 0;
                string message = "";
                if(Int32.TryParse(input, out enteredNumber))
                {
                    switch (enteredNumber)
                    {
                        case (1):
                            message = "Die Zahl ist eins.";
                            choice = enteredNumber;
                            break;
                        case (2):
                            message = "Die Zahl ist zwei.";
                            choice = enteredNumber;
                            break;
                        case (3):
                            message = "Die Zahl ist drei";
                            choice = enteredNumber;
                            break;
                        case (9):
                            message = "Programm wird beendet.";
                            choice = enteredNumber;
                            break;
                        default:
                            message = "Für diese Zahl existiert kein Menüeintrag. Bitte erneut versuchen.";
                            break;
                    }
                }
                else
                {
                    message = "Falsche Eingabe. Bitte erneut versuchen. (deine letzte Eingabe war: " + input + ")";
                }
                Console.Clear();
                Console.WriteLine(message);
            }
            Console.ReadKey();
        }
    }
}
